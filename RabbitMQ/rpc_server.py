import pika
from statistics import mean

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.exchange_declare(exchange='logs', exchange_type='fanout')

result = channel.queue_declare(queue='', exclusive=True)
queue_name = result.method.queue

channel.queue_bind(exchange='logs', queue=queue_name)

print(' [*] Waiting for logs. To exit press CTRL+C')


def callback(ch, method, properties, body):
    standard_deviation = []
    data_bytes = body.decode("utf-8")
    converted_data = list(data_bytes.split(sep=','))
    list_converted_data = list(map(int, converted_data))

    average = mean(list_converted_data)
    for i in list_converted_data:
        standard_deviation.append((i-average)**2)  # Дисперсия значений

    print('Минимальное значение: ', min(list_converted_data))
    print('Максимальное значение: ', max(list_converted_data))
    print('Среднее значение: ', average)
    print('Среднеквадратичное отклонение: ', (sum(standard_deviation) / len(standard_deviation))**0.5)  # Формула СКО


channel.basic_consume(
    queue=queue_name, on_message_callback=callback, auto_ack=True)

channel.start_consuming()
